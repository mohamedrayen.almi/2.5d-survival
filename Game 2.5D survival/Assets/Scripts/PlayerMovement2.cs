using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class PlayerMovement2 : MonoBehaviour
{
    public float step;
    private Rigidbody rb;
    private int score;
    private Vector3 movement;
    public int direction = 1;
    private float diff;
    private float diffy;
    public GameObject cam;
    public float jumpForce = 20.0f;  
    private bool isJumping = false;
    private bool isGrounded = false;  
    public float grav=5;
    public float hitpower = 200f;
    private bool isKnockedBack = false;
    private Vector3 knockbackDirection = Vector3.zero;
    public Canvas UI;
    public Slider healthbar;
    public Slider ScoreBar;
    public GameObject gameover;
    public GameObject YouWin;
    public Animator animatorboy;
    public Animator animatorgirl;
    public Animator animator;
    public AudioSource deathSound;
    public AudioSource hurtSound;
    private bool isMovingRight = false;
    private bool isMovingLeft = false;

    public bool isMobile;
    public GameObject mobileMenu;

    public GameObject boy;
    public GameObject girl;

    string m_DeviceType;
    void Start()
    {
        if (PlayerPrefs.GetInt("caracter") == 0)
        {
            girl.SetActive(false);
            boy.SetActive(true);
            animator = animatorboy;
        }
        else 
        {
            girl.SetActive(true);
            boy.SetActive(false);
            animator = animatorgirl;
        }
        if (isMobile) step = 800;
        else
            step = 600;
        diff = transform.position.x - cam.transform.position.x;
        rb = GetComponent<Rigidbody>();
        Physics.gravity = new Vector3(0, -grav, 0);
        if(!isMobile)
            mobileMenu.SetActive(false);
        Debug.Log(PlayerPrefs.GetInt("soundfx"));
        if (PlayerPrefs.GetInt("soundfx") == 0)
        { 
            deathSound.mute = false;
            hurtSound.mute = false;
        }
        else
        {
            deathSound.mute = true;
            hurtSound.mute = true;
        }
    }


    public void MobileMouvmentRightStart()
    {
        animator.SetFloat("step", 1);
        isMovingRight = true;
        direction = 1;
    }

    public void MobileMouvmentRightEnd()
    {
        animator.SetFloat("step", 0);
        isMovingRight = false;
        movement = Vector3.zero;
    }

    public void MobileMouvmentLeftStart()
    {
        animator.SetFloat("step", -1);
        isMovingLeft = true;
        direction = -1;
    }

    public void MobileMouvmentLeftEnd()
    {
        animator.SetFloat("step", 0);
        isMovingLeft = false;
        movement = Vector3.zero;
    }

    public void Jump()
    {
        if (!isJumping && isGrounded)
        {
        isJumping = true;
        isGrounded = false;
        }
    }

    

    
    void Update()
    {

        healthbar.maxValue = PlayerPrefs.GetInt("health");
        cam.transform.position = new Vector3(transform.position.x + diff, transform.position.y+1.5f, cam.transform.position.z);
        if (!isMobile)
        {
            if (Input.GetButtonDown("Jump"))
            {
                Jump();
            }
            if (!isJumping && isGrounded)
            {
                movement = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
                animator.SetFloat("step", Input.GetAxis("Horizontal"));
                rb.velocity = step * Time.deltaTime * movement;
            }
            if (Input.GetAxis("Horizontal") > 0)
            {
                direction = -1;
                transform.eulerAngles = new Vector3(0, 90, 0);
            }
            else if(Input.GetAxis("Horizontal") < 0)
            {
                transform.eulerAngles = new Vector3(0, -90, 0);
                direction = 1;
            }
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, direction*90, 0);
            if (isMovingRight)
            {
                movement = new Vector3(0.5f, 0, rb.velocity.z);
            }
            if (isMovingLeft)
            {
                movement = new Vector3(-0.5f, 0, rb.velocity.z);
            }
            if (Input.touchCount < 1 && !isJumping && isGrounded)
            {
                movement = Vector3.zero;
            }
            if (!isJumping && isGrounded)
                if (Input.touchCount < 1)
                    rb.velocity = Vector3.zero;
                else
                    rb.velocity = step * Time.deltaTime * movement;
        }
        //if (isGrounded)
        //{
        //    rb.velocity = step * Time.deltaTime * movement;
        //}


        if (healthbar.value == 0)
            {
            PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money")+ ((int)ScoreBar.value * 2));
                deathSound.Play();
                gameover.SetActive(true);
                Destroy(gameObject);
            }
        
        
    }

    public void StopMoving()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

    }

    private void FixedUpdate()
    {
        rb.AddForce(Physics.gravity);
        if (isKnockedBack)
        {
            rb.AddForce(-knockbackDirection * hitpower * Time.fixedDeltaTime, ForceMode.Impulse);
            StartCoroutine(StopKnockback());
        }
        if (isJumping)
        {
            Vector3 jumpVector;
            if (isMobile)
            jumpVector = new Vector3(direction *1, jumpForce, 0f);
            else
            jumpVector = new Vector3(-direction * 1, jumpForce, 0f);
            rb.AddForce(jumpVector, ForceMode.Impulse);
            StartCoroutine(StopJumping());
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Terrain"))
        {
            isJumping = false;
            isGrounded = true;
        }
        else if (collision.gameObject.CompareTag("Enemy") && !isKnockedBack)
        {
            hurtSound.Play();
            healthbar.value--;

            knockbackDirection = -collision.contacts[0].normal;

            isKnockedBack = true;
        }


        if (collision.gameObject.CompareTag("Win"))
        {
            YouWin.SetActive(true);
        }

        if (collision.gameObject.CompareTag("GameOver"))
        {
            deathSound.Play();
            gameover.SetActive(true);
            Destroy(gameObject);
        }

        }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Terrain"))
        {
            isGrounded = true;
        }
    }
    private IEnumerator StopKnockback()
    {
        yield return new WaitForSeconds(0.2f); 
        isKnockedBack = false;
        knockbackDirection = Vector3.zero;
    }

    private IEnumerator StopJumping()
    {
        yield return new WaitForSeconds(0.3f);
        isJumping = false;
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Terrain"))
        {
            isGrounded = false; 
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Contains("Key"))
        {
            Destroy(other.gameObject);
            score++;
            ScoreBar.value = ScoreBar.value + 1;
        }
    }
}
